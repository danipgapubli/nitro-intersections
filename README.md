# Nitro C++ Proficiency Test for Senior Candidates: rectangle intersections

# Problem
 Rectangle intersections

## Build requirements
 * Build tool: MSBuild.
 * Compiler support for C++17.
 * Tested with Visual Studio 2019 v16.1.1.

## Third party libraries
The solution uses the following third party libraries:
 * JSON for Modern C++ (https://nlohmann.github.io/json/), for parsing JSON input.
 * valijson (https://github.com/tristanpenman/valijson), for validating JSON using JSON schema.
 * catch2 (https://github.com/catchorg/Catch2), for testing.

## Usage
```shell
cd <build-dir>
intersections <file.json>
```
### Example of output
```shell
Input:
        1: Rectangle at (100,100), w=250, h=80.
        2: Rectangle at (120,200), w=250, h=150.
        3: Rectangle at (140,160), w=250, h=100.
        4: Rectangle at (160,140), w=350, h=190.
Intersections
        Between rectangle 1 and 3 at (140,160), w=210, h=20.
        Between rectangle 1 and 4 at (160,140), w=190, h=40.
        Between rectangle 2 and 3 at (140,200), w=230, h=60.
        Between rectangle 2 and 4 at (160,200), w=210, h=130.
        Between rectangle 3 and 4 at (160,160), w=230, h=100.
        Between rectangle 1, 3 and 4 at (160,160), w=190, h=20.
        Between rectangle 2, 3 and 4 at (160,200), w=210, h=60.
```
## Candidate
* Daniel Paredes García
* danipga.publi@gmail.com