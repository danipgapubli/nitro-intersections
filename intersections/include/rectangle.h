#ifndef _RECTANGLE_H_
#define _RECTANGLE_H_

#include <cstdint>
#include <cassert>
#include <algorithm>

namespace intersections
{
	struct rectangle
	{
		std::int64_t x;
		std::int64_t y;
		// these are unsigned, but better not mixing operations with signed and unsigned
		std::int64_t width;
		std::int64_t height;

		bool constexpr operator==(rectangle const& rhs) const noexcept
		{
			return x == rhs.x && y == rhs.y
				&& width == rhs.width && height == rhs.height;
		}
		bool constexpr operator!=(rectangle const& rhs) const noexcept
		{
			return !(*this == rhs);
		}

		bool constexpr intersects_with(rectangle const& other) const noexcept
		{
			return
				x <= other.x + other.width
				&& x + width >= other.x
				&& y <= other.y + other.height
				&& y + height >= other.y;
		}

		rectangle constexpr intersection_with(rectangle const& other) const noexcept
		{
			// alternatively: return std::optional<rectangle> for handling case when no intersection
			assert(intersects_with(other));
			auto const ix = std::max(x, other.x);
			auto const iy = std::max(y, other.y);
			return {
				ix,
				iy,
				std::min(x + width, other.x + other.width) - ix,
				std::min(y + height, other.y + other.height) - iy
			};
		}
	};

	static std::ostream& operator<<(std::ostream& os, rectangle const& r)
	{
		os << "{\"x\":" << r.x << ", \"y\":" << r.y
			<< ", \"w\":" << r.width << ", \"h\":" << r.height << '}';
		return os;
	}
}

#endif // _RECTANGLE_H_