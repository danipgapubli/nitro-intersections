#ifndef _RECTANGLE_INTERSECTIONS_H_
#define _RECTANGLE_INTERSECTIONS_H_

#include <vector>
#include <queue>
#include <utility>

#include "rectangle.h"

namespace intersections
{
	template<typename F>
	static void find_intersections(std::vector<rectangle> const& rectangles, F&& func)
	{
		using index_t = std::vector<rectangle>::size_type;

		std::queue<std::pair<rectangle, std::vector<index_t>>> intersections; // intersection + indexes of rectangles that make that intersection
		for (auto i = 0u; i < rectangles.size(); ++i)
			intersections.emplace(std::make_pair(rectangles[i], std::vector<index_t>{i}));

		while (!intersections.empty())
		{
			auto const& current_intersection = intersections.front();
			auto const& rectangle = current_intersection.first;
			auto const& indexes = current_intersection.second;
			for (index_t i = indexes.back() + 1; i < rectangles.size(); ++i)
			{
				if (rectangle.intersects_with(rectangles[i]))
				{
					auto const intersection = rectangle.intersection_with(rectangles[i]);
					std::vector<index_t> intersection_indexes = indexes;
					intersection_indexes.emplace_back(i);

					func(intersection, intersection_indexes);
					intersections.emplace(std::make_pair(std::move(intersection), std::move(intersection_indexes)));
				}
			}
			intersections.pop();
		}
	}
}

#endif
