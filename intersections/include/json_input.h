#ifndef _JSON_INPUT_H_
#define _JSON_INPUT_H_

#include <ostream>
#include <iterator>
#include <algorithm>
#include <optional>
#include <vector>

#include "json.hpp" //should better be: #include "nlohmann/json.hpp", but adapter it's using #include "json.hpp"
#include "valijson/adapters/nlohmann_json_adapter.hpp"
#include "valijson/schema.hpp"
#include "valijson/schema_parser.hpp"
#include "valijson/validator.hpp"
#include "valijson/validation_results.hpp"

#include "rectangle.h"

namespace intersections
{
	static nlohmann::json const schema_json = R"(
		{
			"$schema": "http://json-schema.org/draft-07/schema#",
			"type": "object",
			"required": ["rects"],
			"properties": {
				"rects": {
					"type": "array",
					"items": {
						"type" : "object",
						"required": ["x", "y", "w", "h"],
						"properties": {
							"x" : { "type" : "number" },
							"y" : { "type" : "number" },
							"w" : { "type" : "number" },
							"h" : { "type" : "number" }
						},
						"additionalProperties": false
					}
				}
			},
			"additionalProperties": false
		}
	)"_json;

	static bool is_valid_json(nlohmann::json json, std::ostream& os)
	{
		using namespace valijson;
		Schema json_schema;
		SchemaParser json_schema_parser;
		adapters::NlohmannJsonAdapter schema_adapter(schema_json);
		json_schema_parser.populateSchema(schema_adapter, json_schema);

		adapters::NlohmannJsonAdapter json_adapter(json);
		valijson::Validator validator;
		ValidationResults validation_results;
		auto const is_valid = validator.validate(json_schema, json_adapter, &validation_results);
		if (!is_valid)
		{
			os << "JSON schema validation failed:\n";
			ValidationResults::Error error;
			for (auto number_of_error = 1u; validation_results.popError(error); ++number_of_error)
			{
				os << '\t' << number_of_error << ": ";
				std::copy(std::cbegin(error.context), std::cend(error.context),
					std::ostream_iterator<std::string>(os, " "));
				os << " - " << error.description << '\n';
			}
		}
		return is_valid;
	}

	void from_json(const nlohmann::json& j, rectangle& r)
	{
		j.at("x").get_to(r.x);
		j.at("y").get_to(r.y);
		j.at("w").get_to(r.width);
		j.at("h").get_to(r.height);
	}

	// see nlohmann/json.hpp parse function for the list of valid types,
	// to be precise, look at constructor of detail::input_adapter.
	template<typename T>
	static std::optional<std::vector<rectangle>> get_rectangles(T&& input, std::ostream& os)
	{
		auto const json_rects = nlohmann::json::parse(input);
		if (is_valid_json(json_rects, os))
			return json_rects.at("rects").get<std::vector<rectangle>>();
		return {};
	}
}

#endif