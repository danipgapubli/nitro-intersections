#include <iostream>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <vector>
#include <queue>
#include <cerrno>
#include <cstring>
#include <cassert>
#include <utility>
#include <algorithm>
#include <exception>

#include "rectangle.h"
#include "rectangle_intersections.h"
#include "json_input.h"

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		std::cerr << "Error: missing mandatory parameter\n"
			<< "Usage: " << std::filesystem::path(argv[0]).filename().string() << " <file.json>";
		return EXIT_FAILURE;
	}
	using namespace intersections;

	auto const json_filepath = argv[1];
	std::ifstream json_ifs(json_filepath);

	if (!json_ifs)
	{
		std::cerr << "Error: error opening file '" << json_filepath << "\': " << std::strerror(errno) << '\n';
		return EXIT_FAILURE;
	}

	try
	{
		auto const rectangles_opt = get_rectangles(json_ifs, std::cerr);
		if (!rectangles_opt)
			return EXIT_FAILURE;

		auto const& rectangles = *rectangles_opt;
		std::cout << "Input:\n";
		{
			auto i = 1u;
			for (auto const& rect : rectangles)
			{
				std::cout
					<< '\t' << i << ": Rectangle at (" << rect.x << ',' << rect.y << "),"
					<< " w=" << rect.width << ", h=" << rect.height << ".\n";
				++i;
			}
		}

		std::cout << "Intersections\n";
		find_intersections(
			rectangles,
			[](rectangle const & intersection, auto const & indexes)
			{
				assert(indexes.size() >= 2);
				std::cout << "\tBetween rectangle " << *std::cbegin(indexes) + 1;
				auto const last_index_it = std::prev(std::cend(indexes));
				for (auto it = std::next(std::cbegin(indexes)); it != last_index_it; ++it)
					std::cout << ", " << *it + 1;
				std::cout 
					<< " and " << *last_index_it + 1
					<< " at (" << intersection.x << ',' << intersection.y << "),"
					<< " w=" << intersection.width << ", h=" << intersection.height << ".\n";
			}
		);
	}
	catch (std::exception& e)
	{
		std::cerr << "Error: exception - " << e.what() << '\n';
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}