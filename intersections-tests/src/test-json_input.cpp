#include <catch2/catch.hpp>

#include <sstream>
#include <vector>

#include "json_input.h"

using namespace intersections;

TEST_CASE("Valid input", "[json_input]")
{
	std::istringstream iss(R"(
	{ "rects":[
		{"x":100,"y":100,"w":250,"h":80},
		{"x":120,"y":200,"w":250,"h":150},
		{"x":140,"y":160,"w":250,"h":100},
		{"x":160,"y":140,"w":350,"h":190}]
	})");
	std::vector<rectangle> expected =
	{
		{100,100,250,80},
		{120,200,250,150},
		{140,160,250,100},
		{160,140,350,190}
	};
	std::ostringstream oss;
	auto const rectangles_opt = get_rectangles(iss, oss);
	CHECK(rectangles_opt.has_value());
	CHECK(*rectangles_opt == expected);
}

TEST_CASE("Valid input - different order", "[json_input]")
{
	std::istringstream iss(R"(
	{ "rects":[
		{"x":100,"y":100,"w":250,"h":80},
		{"y":200,"x":120,"w":250,"h":150},
		{"w":250,"x":140,"y":160,"h":100},
		{"h":190,"x":160,"y":140,"w":350}]
	})");
	std::vector<rectangle> expected =
	{
		{100,100,250,80},
		{120,200,250,150},
		{140,160,250,100},
		{160,140,350,190}
	};
	std::ostringstream oss;
	auto const rectangles_opt = get_rectangles(iss, oss);
	CHECK(rectangles_opt.has_value());
	CHECK(*rectangles_opt == expected);
}

TEST_CASE("Valid input, empty array", "[json_input]")
{
	std::istringstream iss(R"({ "rects":[] } )");
	std::vector<rectangle> expected = {};
	std::ostringstream oss;
	auto const rectangles_opt = get_rectangles(iss, oss);
	CHECK(rectangles_opt.has_value());
	CHECK(*rectangles_opt == expected);
}

TEST_CASE("Invalid input, missing \"x\"", "[json_input]")
{
	std::istringstream iss(R"(
	{ "rects":[
		{        "y":100,"w":250,"h":80},
		{"x":120,"y":200,"w":250,"h":150},
		{"x":140,"y":160,"w":250,"h":100},
		{"x":160,"y":140,"w":350,"h":190}]
	})");
	std::ostringstream oss;
	auto const rectangles_opt = get_rectangles(iss, oss);
	CHECK(!rectangles_opt.has_value());
}

TEST_CASE("Invalid input, unexpected extra property \"z\"", "[json_input]")
{
	std::istringstream iss(R"(
	{ "rects":[
		{"x":100,"y":100,"w":250,"h":80,"z":0},
		{"x":120,"y":200,"w":250,"h":150},
		{"x":140,"y":160,"w":250,"h":100},
		{"x":160,"y":140,"w":350,"h":190}]
	})");
	std::ostringstream oss;
	auto const rectangles_opt = get_rectangles(iss, oss);
	CHECK(!rectangles_opt.has_value());
}

TEST_CASE("Invalid input, no array", "[json_input]")
{
	std::istringstream iss(R"(
	{ 
		"rects": {"x":100,"y":100,"w":250,"h":80,"z":0}
	})");
	std::ostringstream oss;
	auto const rectangles_opt = get_rectangles(iss, oss);
	CHECK(!rectangles_opt.has_value());
}