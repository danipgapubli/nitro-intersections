#include <catch2/catch.hpp>

#include "rectangle.h"

using namespace intersections;

TEST_CASE("comparison operators", "[rectangle][operator==]")
{
	CHECK(rectangle{1,2,3,4} == rectangle{1,2,3,4});
	CHECK((rectangle{1,2,3,4} != rectangle{1,2,3,4}) == false);
	CHECK((rectangle{1,2,3,4} == rectangle{2,2,3,4}) == false);
	CHECK(rectangle{1,2,3,4} != rectangle{2,2,3,4});
}

static const rectangle base_r = { 1, 1, 2, 2 };

TEST_CASE("Top left intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(base_r.intersects_with({ 0, 0, 2, 2 }));
}
TEST_CASE("Top right intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(base_r.intersects_with({ 2, 0, 2, 2 }));
}
TEST_CASE("Bottom left intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(base_r.intersects_with({ 0, 2, 2, 2 }));
}
TEST_CASE("Bottom right intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(base_r.intersects_with({ 2, 2, 2, 2 }));
}
TEST_CASE("No intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(base_r.intersects_with(rectangle{10, 10, 1, 1}) == false);
}
TEST_CASE("Top wide intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(base_r.intersects_with(rectangle{0, 0, 10, 10}));
}
TEST_CASE("Top narrow intersection", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(rectangle{ 1, 1, 10, 10 }.intersects_with(rectangle{2, 0, 5, 5}));
}
TEST_CASE("One rectangle with negative axis interception", "[rectangle][intersects_with][simple-cases]")
{
	CHECK(rectangle{ -1, -1, 10, 10 }.intersects_with(rectangle{1, 1, 5, 5}));
}

TEST_CASE("Intersects in corners", "[rectangle][intersects_with][corner-cases]")
{
	CHECK(base_r.intersects_with(rectangle{0, 0, 1, 1}));
	CHECK(base_r.intersects_with(rectangle{3, 0, 1, 1}));
	CHECK(base_r.intersects_with(rectangle{0, 3, 1, 1}));
	CHECK(base_r.intersects_with(rectangle{3, 3, 1, 1}));
}
TEST_CASE("Intersects with sides", "[rectangle][intersects_with][corner-cases]")
{
	CHECK(base_r.intersects_with(rectangle{0, 1, 2, 2}));
	CHECK(base_r.intersects_with(rectangle{1, 0, 2, 2}));
	CHECK(base_r.intersects_with(rectangle{3, 1, 2, 2}));
	CHECK(base_r.intersects_with(rectangle{1, 3, 2, 2}));
}
TEST_CASE("Intersects including sides", "[rectangle][intersects_with][corner-cases]")
{
	CHECK(base_r.intersects_with(rectangle{ 0, 1, 4, 4 }));
	CHECK(base_r.intersects_with(rectangle{ 1, 0, 4, 4 }));
	CHECK(base_r.intersects_with(rectangle{ 3, 1, 4, 4 }));
	CHECK(base_r.intersects_with(rectangle{ 1, 3, 4, 4 }));
}
TEST_CASE("Intersects same rectangle", "[rectangle][intersects_with][corner-cases]")
{
	CHECK(base_r.intersects_with(base_r));
	CHECK(base_r.intersects_with(rectangle{ base_r }));
}
TEST_CASE("Intersects inside rectangle", "[rectangle][intersects_with][corner-cases]")
{
	CHECK(base_r.intersects_with(rectangle{ 0,0, 5,5 }));
}

// From example in problem description
static const rectangle r1 = { 100, 100, 250, 80 };
static const rectangle r2 = { 120, 200, 250, 150 };
static const rectangle r3 = { 140, 160, 250, 100 };
static const rectangle r4 = { 160, 140, 350, 190 };

TEST_CASE("Example intersects rectangles 1,2", "[rectangle][intersects_with][example]")
{
	CHECK(r1.intersects_with(r2) == false);
}
TEST_CASE("Example intersects rectangles 1,3", "[rectangle][intersects_with][example]")
{
	CHECK(r1.intersects_with(r3));
}
TEST_CASE("Example intersects rectangles 1,4", "[rectangle][intersects_with][example]")
{
	CHECK(r1.intersects_with(r4));
}
TEST_CASE("Example intersects rectangles 2,3", "[rectangle][intersects_with][example]")
{
	CHECK(r2.intersects_with(r3));
}
TEST_CASE("Example intersects rectangles 2,4", "[rectangle][intersects_with][example]")
{
	CHECK(r2.intersects_with(r4));
}
TEST_CASE("Example intersects rectangles 3,4", "[rectangle][intersects_with][example]")
{
	CHECK(r3.intersects_with(r4));
}
TEST_CASE("Example intersects rectangles 1,2 and 2,1 (commutative)", "[rectangle][intersects_with][commutative][example]")
{
	CHECK(r2.intersects_with(r1) == false);
	CHECK(r1.intersects_with(r2) == r2.intersects_with(r1));
}
TEST_CASE("Example intersects rectangles 1,3 and 3,1 (commutative)", "[rectangle][intersects_with][commutative][example]")
{
	CHECK(r3.intersects_with(r1));
	CHECK(r1.intersects_with(r3) == r3.intersects_with(r1));
}

TEST_CASE("Intersection in corners", "[rectangle][intersects_with][corner-cases]")
{
	CHECK(base_r.intersection_with(rectangle{ 0, 0, 1, 1 }) == rectangle{1,1,0,0});
	CHECK(base_r.intersection_with(rectangle{ 3, 0, 1, 1 }) == rectangle{3,1,0,0});
	CHECK(base_r.intersection_with(rectangle{ 0, 3, 1, 1 }) == rectangle{1,3,0,0});
	CHECK(base_r.intersection_with(rectangle{ 3, 3, 1, 1 }) == rectangle{3,3,0,0});
}
TEST_CASE("Intersection with sides", "[rectangle][intersection_with][corner-cases]")
{
	CHECK(base_r.intersection_with(rectangle{ 0, 1, 1, 2 }) == rectangle{1,1,0,2});
	CHECK(base_r.intersection_with(rectangle{ 1, 0, 2, 1 }) == rectangle{1,1,2,0});
	CHECK(base_r.intersection_with(rectangle{ 3, 1, 2, 2 }) == rectangle{3,1,0,2});
	CHECK(base_r.intersection_with(rectangle{ 1, 3, 2, 2 }) == rectangle{1,3,2,0});
}
TEST_CASE("Intersection including sides", "[rectangle][intersection_with][corner-cases]")
{
	CHECK(base_r.intersection_with(rectangle{ 0, 1, 1, 4 }) == rectangle{1,1,0,2});
	CHECK(base_r.intersection_with(rectangle{ 1, 0, 4, 1 }) == rectangle{1,1,2,0});
	CHECK(base_r.intersection_with(rectangle{ 3, 1, 1, 4 }) == rectangle{3,1,0,2});
	CHECK(base_r.intersection_with(rectangle{ 1, 3, 4, 1 }) == rectangle{1,3,2,0});
}
TEST_CASE("Intersection same rectangle", "[rectangle][intersection_with][corner-cases]")
{
	CHECK(base_r.intersection_with(base_r) == base_r);
	CHECK(base_r.intersection_with(rectangle{ base_r }) == base_r);
}
TEST_CASE("Intersection inside rectangle", "[rectangle][intersection_with][corner-cases]")
{
	CHECK(base_r.intersection_with(rectangle{ 0,0, 5,5 }) == base_r);
}

TEST_CASE("Example intersection rectangles 1,3 (commutative)", "[rectangle][intersection_with][example]")
{
	auto const intersection = rectangle{ 140,160,210,20 };
	CHECK(r1.intersection_with(r3) == intersection);
	CHECK(r3.intersection_with(r1) == intersection);
}
TEST_CASE("Example intersection rectangles 1,4 (commutative)", "[rectangle][intersection_with][example]")
{
	auto const intersection = rectangle{ 160,140,190,40 };
	CHECK(r1.intersection_with(r4) == intersection);
	CHECK(r4.intersection_with(r1) == intersection);
}
TEST_CASE("Example intersection rectangles 2,3", "[rectangle][intersection_with][example]")
{
	CHECK(r2.intersection_with(r3) == rectangle{ 140,200,230,60 });
}
TEST_CASE("Example intersection rectangles 2,4", "[rectangle][intersection_with][example]")
{
	CHECK(r2.intersection_with(r4) == rectangle{ 160,200,210,130 });
}
TEST_CASE("Example intersection rectangles 3,4", "[rectangle][intersection_with][example]")
{
	CHECK(r3.intersection_with(r4) == rectangle{ 160,160,230,100 });
}
TEST_CASE("Example intersection rectangles 1,3,4", "[rectangle][intersection_with][example]")
{
	CHECK(r1.intersection_with(r3).intersection_with(r4) == rectangle{ 160,160,190,20 });
}
TEST_CASE("Example intersection rectangles 2,3,4", "[rectangle][intersection_with][example]")
{
	CHECK(r2.intersection_with(r3).intersection_with(r4) == rectangle{ 160,200,210,60 });
}
