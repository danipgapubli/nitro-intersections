#include <catch2/catch.hpp>

#include <deque>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iterator>

#include "rectangle_intersections.h"

using namespace intersections;

using index_t = std::vector<rectangle>::size_type;

std::ostream& operator<<(std::ostream& os, std::vector<index_t> indexes)
{
	std::copy(std::cbegin(indexes), std::cend(indexes),
		std::ostream_iterator<index_t>(os, " "));
	return os;
}

static void check_intersection(
	rectangle const& intersection, 
	std::vector<index_t> const& indexes, 
	std::deque<std::pair<std::vector<index_t>, rectangle>> & expected_result)
{
	assert(indexes.size() >= 2);	
	auto const intersection_it = std::find(std::cbegin(expected_result), std::cend(expected_result),
		std::make_pair(indexes, intersection));
	if (intersection_it != std::cend(expected_result))
		expected_result.erase(intersection_it);
	else
	{
		std::ostringstream oss;
		oss << "Unexpected intersection found between rectangles: " << indexes << "with intersection: " << intersection;
		FAIL_CHECK(oss.str());
	}
}

static void report_intersections_not_found(std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result)
{
	for (auto const& missed : expected_result)
	{
		std::ostringstream oss;
		oss << "Intersection not found between rectangles: " << missed.first << "with intersection: " << missed.second;
		FAIL_CHECK(oss.str());
	}
}

TEST_CASE("No rectangles", "[intersections]")
{
	std::vector<rectangle> const rectangles = {};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result = {};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}
TEST_CASE("One single rectangle", "[intersections]")
{
	std::vector<rectangle> const rectangles = { rectangle{1,1,1,1} };
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result = {};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}

TEST_CASE("4 rectangles with no intersections", "[intersections]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 0, 0, 2, 2 },
		rectangle{ 4, 0, 2, 2 },
		rectangle{ 0, 4, 2, 2 },
		rectangle{ 4, 4, 2, 2 },
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result = {};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}
TEST_CASE("One rectangle intersecting with 4 rectangles, one from each", "[intersections]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 1, 1, 4, 4 },
		rectangle{ 0, 0, 2, 2 },
		rectangle{ 4, 0, 2, 2 },
		rectangle{ 0, 4, 2, 2 },
		rectangle{ 4, 4, 2, 2 },
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result =
	{
		std::make_pair(std::vector<index_t>{0,1}, rectangle{1,1,1,1}),
		std::make_pair(std::vector<index_t>{0,2}, rectangle{4,1,1,1}),
		std::make_pair(std::vector<index_t>{0,3}, rectangle{1,4,1,1}),
		std::make_pair(std::vector<index_t>{0,4}, rectangle{4,4,1,1}),
	};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}

TEST_CASE("3 rectangles overlapping in chain", "[intersections]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 0, 0, 3, 3 },
		rectangle{ 1, 0, 3, 3 },
		rectangle{ 2, 0, 3, 3 },
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result =
	{
		std::make_pair(std::vector<index_t>{0,1}, rectangle{1,0,2,3}),
		std::make_pair(std::vector<index_t>{0,2}, rectangle{2,0,1,3}),
		std::make_pair(std::vector<index_t>{1,2}, rectangle{2,0,2,3}),
		std::make_pair(std::vector<index_t>{0,1,2}, rectangle{2,0,1,3}),
	};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}

TEST_CASE("2 identical rectangles", "[intersections]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 0, 0, 2, 2 },
		rectangle{ 0, 0, 2, 2 },
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result = 
	{
		std::make_pair(std::vector<index_t>{0,1}, rectangle{0,0,2,2}),
	};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}
TEST_CASE("Rectangle inside rectangle", "[intersections]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 0, 0, 3, 3 },
		rectangle{ 1, 1, 1, 1 },
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result = 
	{
		std::make_pair(std::vector<index_t>{0,1}, rectangle{1,1,1,1}),
	};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}

TEST_CASE("Intersections provided in problem description", "[intersections][example]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 100, 100, 250, 80 },
		rectangle{ 120, 200, 250, 150 },
		rectangle{ 140, 160, 250, 100 },
		rectangle{ 160, 140, 350, 190 }
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result =
	{
		std::make_pair(std::vector<index_t>{0,2}, rectangle{140,160,210,20}),
		std::make_pair(std::vector<index_t>{0,3}, rectangle{160,140,190,40}),
		std::make_pair(std::vector<index_t>{1,2}, rectangle{140,200,230,60}),
		std::make_pair(std::vector<index_t>{1,3}, rectangle{160,200,210,130}),
		std::make_pair(std::vector<index_t>{2,3}, rectangle{160,160,230,100}),
		std::make_pair(std::vector<index_t>{0,2,3}, rectangle{160,160,190,20}),
		std::make_pair(std::vector<index_t>{1,2,3}, rectangle{160,200,210,60}),
	};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}
TEST_CASE("Intersections provided in problem description, adding duplicated rectangles", "[intersections]")
{
	std::vector<rectangle> const rectangles = {
		rectangle{ 100, 100, 250, 80 },
		rectangle{ 120, 200, 250, 150 },
		rectangle{ 140, 160, 250, 100 },
		rectangle{ 160, 140, 350, 190 },
		rectangle{ 100, 100, 250, 80 }, // same as rectangles[0]
	};
	std::deque<std::pair<std::vector<index_t>, rectangle>> expected_result =
	{
		std::make_pair(std::vector<index_t>{0,2}, rectangle{140,160,210,20}),
		std::make_pair(std::vector<index_t>{0,3}, rectangle{160,140,190,40}),
		std::make_pair(std::vector<index_t>{0,4}, rectangle{100,100,250,80}),
		std::make_pair(std::vector<index_t>{1,2}, rectangle{140,200,230,60}),
		std::make_pair(std::vector<index_t>{1,3}, rectangle{160,200,210,130}),
		std::make_pair(std::vector<index_t>{2,3}, rectangle{160,160,230,100}),
		std::make_pair(std::vector<index_t>{2,4}, rectangle{140,160,210,20}),
		std::make_pair(std::vector<index_t>{3,4}, rectangle{160,140,190,40}),
		std::make_pair(std::vector<index_t>{0,2,3}, rectangle{160,160,190,20}),
		std::make_pair(std::vector<index_t>{0,2,4}, rectangle{140,160,210,20}),
		std::make_pair(std::vector<index_t>{0,3,4}, rectangle{160,140,190,40}),
		std::make_pair(std::vector<index_t>{1,2,3}, rectangle{160,200,210,60}),
		std::make_pair(std::vector<index_t>{2,3,4}, rectangle{160,160,190,20}),
		std::make_pair(std::vector<index_t>{0,2,3,4}, rectangle{160,160,190,20}),
	};
	find_intersections(
		rectangles,
		[&](rectangle const& intersection, auto const& indexes)
		{
			check_intersection(intersection, indexes, expected_result);
		}
	);
	CHECK(expected_result.empty());
	if (!expected_result.empty())
	{
		report_intersections_not_found(expected_result);
	}
}
